#include<stdio.h>
void swap(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
}
int main()
{
int n1,n2;
printf("Enter the two numbers");
scanf("%d %d",&n1,&n2);
printf(" \n value of n1 and n2 before function call is %d %d ",n1,n2);
swap(&n1,&n2);
printf(" \n Value of n1 and n2 after swapping is %d %d ",n1,n2);
return 0;
}